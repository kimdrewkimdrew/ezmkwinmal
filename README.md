# Easy Make Windows Malware

# Usage (version2, 3)
import ezmkwinmal

# Requirements
requests,
pyinstaller,
pywin32,
pyHook,
pyshark(wireshark = dumpcap.exe,tshark.exe)

# v1.4 added
SetUAC(),
RegisterTask(),
SelfHide(),
PortScan()

# v1.3 added
StringInject(),
EditRegistry(),
TrustedInstaller(),
PacketCapture()

# v1.2 added
FileDownloader(),
GetPrivs(),
RandomKeyGenerator(),
RunAsSystem(),
SelfCompilePyCode()

# v1.1 added
RunAsAdmin(),
SetFireWall(),
SetWindowsDefender()

# v1.0 added
WindowSize(),
SelfCopy(),
SelfMove(),
FileEncryption(),
KeyLogging()



